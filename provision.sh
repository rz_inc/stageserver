
## Init
#yum update -y
yum install -y java-1.7.0-openjdk wget git

## Jenkins
wget http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo -O /etc/yum.repos.d/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
yum install -y jenkins
chkconfig jenkins on
systemctl start jenkins
ls -al /var/lib/jenkins/

sudo -u jenkins mkdir -p /var/lib/jenkins/nginx/conf.d
sudo -u jenkins touch /var/lib/jenkins/nginx/conf.d/jenkins.conf
sudo -u jenkins touch /var/lib/jenkins/nginx/conf.d/deployed.conf
sudo sh -c 'cat << EOF > /var/lib/jenkins/nginx/conf.d/jenkins.conf

upstream jenkins.stg.jp {
    server 172.17.42.1:8080;
}
server {
    server_name jenkins.stg.jp;
    listen 80 ;
    access_log /var/log/nginx/access.log;
    location / {
        proxy_pass http://jenkins.stg.jp;
    }
}
EOF'

## Security
firewall-cmd --permanent --zone=public --add-port=8080/tcp
firewall-cmd --permanent --zone=trusted --change-interface=docker0
firewall-cmd --permanent --zone=trusted --add-port=2376/tcp
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --reload
sed -i 's/SELINUX=disabled/SELINUX=enforcing/' /etc/selinux/config
restorecon -Rv /

## Docker
sed -i 's|^OPTIONS='\''--selinux-enabled'\''|OPTIONS='\''--selinux-enabled -H 127.0.0.1:4243 -H unix:///var/run/docker.sock'\''|m' /etc/sysconfig/docker
service docker restart
docker run -d \
  -p 80:80 \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  -v /var/lib/jenkins/nginx/conf.d/jenkins.conf:/etc/nginx/conf.d/jenkins.conf:ro \
  -v /var/lib/jenkins/nginx/conf.d/deployed.conf:/etc/nginx/conf.d/deployed.conf:ro \
  --name nginx-proxy --security-opt=label:type:docker_t jwilder/nginx-proxy
docker pull centos:centos6
docker pull mysql:5.5
docker build --no-cache --rm -t rrrz/centos6_php54 docker/templates/centos6_php54/conf/
#docker build --no-cache --rm -t rrrz/emptyfuelphp docker/templates/fuelphp/conf/
docker build --no-cache --rm -t rrrz/stg_fuelphp docker/templates/fuelphp_stg/conf/
docker run -p 33061:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root -d docker.io/mysql:5.6

## WEB Application
sudo yum install -y mariadb


### ----------------
### memo
## vagrant-user
wget http://localhost:8080/jnlpJars/jenkins-cli.jar
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin docker-build-publish
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin postbuild-task
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin role-strategy
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin docker
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin custom-tools-plugin
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin global-post-script
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin config-file-provider
#java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin groovy

java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin git
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin backup
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin credentials
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin credentials-binding
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin simple-theme-plugin
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin envinject
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin managed-scripts
java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin postbuildscript
java -jar jenkins-cli.jar -s http://localhost:8080 restart

sudo -u jenkins mkdir -p /var/lib/jenkins/userContent/ModernJenkins-Theme
sudo -u jenkins wget https://raw.githubusercontent.com/mikepenz/ModernJenkins-Theme/master/master.css -O /var/lib/jenkins/userContent/ModernJenkins-Theme/master.css
sudo -u jenkins mkdir -p /var/lib/jenkins/userContent/jenkins-clean-theme
sudo -u jenkins wget https://raw.githubusercontent.com/Dakota628/jenkins-clean-theme/master/master.css -O /var/lib/jenkins/userContent/jenkins-clean-theme/master.css

