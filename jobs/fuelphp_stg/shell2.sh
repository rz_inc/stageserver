### ===================
### Prepare for project
### ===================
FUEL_ENV=`docker exec -i $BUILD_TAG printenv FUEL_ENV`

#docker exec -i $BUILD_TAG yum install -y nodejs npm --enablerepo=epel
docker exec -u projects -i $BUILD_TAG git clone --recursive git://github.com/kriansa/fuel-less.git fuel/packages/less
docker exec -u projects -i $BUILD_TAG mkdir fuel/app/tmp
docker exec -u projects -i $BUILD_TAG mkdir public/assets/img/cache
docker exec -i $BUILD_TAG chmod +x fuel/packages/less/vendor/node/linux/node
docker exec -i $BUILD_TAG chmod g+w fuel/app/tmp
docker exec -i $BUILD_TAG chmod g+w public/assets/img/cache
docker exec -i $BUILD_TAG chmod g+w fuel/app/config/$FUEL_ENV

docker exec -u projects -i $BUILD_TAG env FUEL_ENV=$FUEL_ENV oil r migrate
docker exec -u projects -i $BUILD_TAG env FUEL_ENV=$FUEL_ENV oil r migrate_indexes:up