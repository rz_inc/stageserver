# FuelPHP Job

# Container Spec
### OS

* CentOS6.6

### FuelPHP

* version: `1.7.2`
* composer

### Services

* Apache
* Sendmail

# Configure

* プロジェクト名: 任意
* 説明: 任意
* 実行環境の準備: `on`
    * Jenkinsの環境変数を使用: `on`
    * Jenkinsのビルド変数を使用: `on`
    * スクリプト: container1.shの内容
* ビルドのパラメータ化: `on`
    * パラメーターの追加: `選択`
        * 名前: `SENDER_EMAIL`
        * 選択値: 任意 （例：`noreply@yourdomain.jp`）
    * パラメーターの追加: `選択`
        * 名前: `SENDER_NAME`
        * 選択値: 任意 （例：`yourdomain事務局`）
    * パラメーターの追加: `選択`
        * 名前: `DEPLOY_DOMAIN`
        * 選択値: 任意 （例：`yourdomain.jp`）
* ソースコード管理: `Git`
    * Repositories
        * Repository URL: 任意　（例：`git@bitbucket.org:ACCOUNT/PROJECT.git`）
        * Credential: 任意
    * Branches to build
        * Branch Specifier (blank for 'any'): 任意　（例：`*/release/production`）
    * Additional Behaviours
        * 追加: `Check out to a sub directory`
            * Local subdirectory for repo: `src`
* ビルド・トリガ
    * リモートからビルド: `on`
        * 認証トークン: 任意　（例：`RhpNs2NmAtyiXexDHwVpthknHHpgSGywRHBj`）
* ビルド環境
    * Use secret text(s) or file(s): `on`
        * 追加: `Secret file`
            * Variable: `id_rsa`
            * Credentials: `Specific Credentials`
            * File: Please Upload
    * ビルドプロセスに環境変数をインジェクト: `on`
        * スクリプト: env1.shの内容
* ビルド
    * ビルド手順の追加: `環境変数のインジェクト`
        * プロパティファイルのパス: `$WORKSPACE/props/script.sh`
    * ビルド手順の追加: `Execute managed script`
        * script: `Build: Set up FuelPHP -`
    * ビルド手順の追加: `シェルの実行`
        * シェルスクリプト: shell2.shの内容
    * ビルド手順の追加: `シェルの実行`
        * シェルスクリプト: shell3.shの内容
* ビルド後の処理
    * ビルド後の処理の追加: `Execute a set of scripts`
        * Build steps
            * Add build step: `Execute managed script`
                * script: `Post: Deploy FuelPHP -`
            * Add build step: `Execute managed script`
                * script: `Post: Deployed Nginx Proxy -`
        * Execute script only if build succeeds: `on`

# Add Config File Management

`/configfiles/selectProvider`

* Add a new Config
    * Select the file type you want to create: `Managed script file`
    * Name: Build: `Set up FuelPHP`
    * Content: shell1.shの内容
* Add a new Config
    * Select the file type you want to create: `Managed script file`
    * Name: Build: `Post: Deploy FuelPHP`
    * Content: post_shell1.shの内容
* Add a new Config
    * Select the file type you want to create: `Managed script file`
    * Name: Build: `Post: Deployed Nginx Proxy`
    * Content: post_shell2.shの内容