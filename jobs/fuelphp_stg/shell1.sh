### ===================
### Set up FuelPHP
### ===================

# variables
#export -p

# common directory
touch $WORKSPACE/conf/docker/id_rsa
cat $id_rsa > $WORKSPACE/conf/docker/id_rsa
touch $WORKSPACE/conf/docker/env
export -p > $WORKSPACE/conf/docker/env

# prepare
docker exec -u projects -i $BUILD_TAG bash -c 'cat /home/projects/jenkins/id_rsa > /home/projects/.ssh/id_rsa'
docker exec -u projects -i $BUILD_TAG git init
docker exec -u projects -i $BUILD_TAG git remote add origin $GIT_URL
docker exec -u projects -i $BUILD_TAG git fetch origin $GIT_BRANCH_NAME
docker exec -u projects -i $BUILD_TAG git reset --hard origin/$GIT_BRANCH_NAME

# configure
docker exec -u projects -i $BUILD_TAG bash -c 'source /home/projects/jenkins/env && /home/projects/configure.sh'

# initialize
docker exec -u projects -i $BUILD_TAG php composer.phar update

# db
mysql -h 127.0.0.1 -P 33061 -u root -proot -e "create database $DBNAME default charset=utf8;"

# run
docker exec -i $BUILD_TAG sed -i -e "18i SetEnv FUEL_ENV $FUEL_ENV" /etc/httpd/conf.d/php.conf
docker exec -i $BUILD_TAG service httpd reload
docker logs $BUILD_TAG