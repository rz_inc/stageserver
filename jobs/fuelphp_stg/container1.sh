WORKSPACE=$JENKINS_HOME/jobs/$JOB_NAME/workspace

### ===================
### Clear old containers
### ===================
if [ `docker ps -aq --filter "name=jenkins-${JOB_NAME}-"` ]; then 
  docker rm -f `docker ps -aq --filter "name=jenkins-${JOB_NAME}-"`
fi

### ===================
### Run a new container
### ===================
mkdir -p $WORKSPACE/conf/docker
docker run -d \
-e VIRTUAL_HOST=$BUILD_TAG.stg.jp \
-e DEPLOY_HOST=$DEPLOY_DOMAIN  \
-e FUEL_ENV=production \
-e JENKINS_BUILD_TAG=$BUILD_TAG \
-w /home/projects/project \
-v $WORKSPACE/conf/docker:/home/projects/jenkins \
--privileged=true \
--name $BUILD_TAG rrrz/fuelphp_stg


### ===================
### Open Permission for common directory
### ===================
sudo chown jenkins.$USER_ID -R $WORKSPACE/conf/docker
sudo chmod g+rw -R $WORKSPACE/conf/docker
sudo chmod g+s $WORKSPACE/conf/docker


### ===================
### Properties
### ===================
mkdir -p $WORKSPACE/props/
touch $WORKSPACE/props/script.sh
chmod u+x $WORKSPACE/props/script.sh