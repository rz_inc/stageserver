id=`docker inspect --format '{{ .Id }}' $BUILD_TAG`
FUEL_ENV=`docker exec -i $BUILD_TAG printenv FUEL_ENV`
GIT_BRANCH_NAME=`echo "$GIT_BRANCH" | sed "s/origin\///"`
DBNAME_old=jenkins_${JOB_NAME}_${BUILD_NUMBER}_${FUEL_ENV}
DBNAME_new=deployed_${JOB_NAME}_${FUEL_ENV}
CONTAINER_new=deployed-$JOB_NAME

### ===================
### Deploy
### ===================
if [ `docker ps -aq --filter "name=deployed-${JOB_NAME}"` ]; then 
  ### Update current container's sources
  docker exec -u projects -i $CONTAINER_new git pull origin $GIT_BRANCH_NAME 
  docker exec -u projects -i $CONTAINER_new oil r migrate
   build_tag=`docker inspect --format '{{range .Config.Env}}{{println .}}{{end}}' $CONTAINER_new | grep JENKINS_BUILD_TAG= | sed 's/^JENKINS_BUILD_TAG=//'`
else
  ### Rename container
  docker rename $BUILD_TAG $CONTAINER_new
  build_tag=$BUILD_TAG
  mysql -h 127.0.0.1 -P 33061 -u root -proot -e "create database $DBNAME_new charset=utf8;"
  docker exec -i $CONTAINER_new sed -i "s/$DBNAME_old/$DBNAME_new/"     fuel/app/config/$FUEL_ENV/db.php
  docker exec -u projects -i $CONTAINER_new rm fuel/app/config/production/migrations.php
  docker exec -u projects -i $CONTAINER_new oil r migrate
  docker exec -u projects -i $CONTAINER_new oil r migrate_indexes:up
fi

### ===================
### Set Nginx-Proxy
### ===================
#export -p
  docker exec -i nginx-proxy sed -i "s/$build_tag.stg.jp/$DEPLOY_DOMAIN/g" /etc/nginx/conf.d/default.conf
  docker exec -i nginx-proxy service nginx reload
  sudo chmod g+rwx /var/lib/docker/containers/$id
  sudo chmod g+rw /var/lib/docker/containers/$id/config.json
  sed -i "s/$build_tag.stg.jp/$DEPLOY_DOMAIN/g" /var/lib/docker/containers/$id/config.json


### ===================
### Clear
### ===================
mysql -h 127.0.0.1 -P 33061 -u root -proot -e "drop database $DBNAME_old;"