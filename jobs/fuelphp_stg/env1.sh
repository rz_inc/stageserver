alias random20='cat /dev/urandom | tr -dc '[:alnum:]' | head -c 20'
FUEL_ENV=`docker exec -i $BUILD_TAG printenv FUEL_ENV`
cat << EOF > $WORKSPACE/props/script.sh
USER_ID=`docker exec -i $BUILD_TAG id -g projects`
FUEL_ENV=${FUEL_ENV}
DBNAME=jenkins_${JOB_NAME}_${BUILD_NUMBER}_${FUEL_ENV}
GIT_BRANCH_NAME=`echo "$GIT_BRANCH" | sed "s/origin\///"`
CRYPTO_KEY=`random20`
CRYPTO_IV=`random20`
CRYPTO_HMAC=`random20`
DB_HOST=172.17.42.1
DB_PORT=33061
DB_USER=root
DB_PASS=root
EOF