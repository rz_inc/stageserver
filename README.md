# Rz Staging Server

# Host
- - - -

## CentOS7
* User
    * jenkins: has DELEGATING sudo (visudo, chown, chmod, chgrp)
    * vagrant
* SELinux: `Enforcing`
* Firewall
    * Inbound: HTTP(80), HTTPS, SSH(20)

## Jenkins
* URL: `http://jenkins.stg.jp/`
* Nginx-Proxy Configure: `/var/lib/jenkins/nginx/conf.d/jenkins.conf`

## Docker
* Nginx-Proxy Container
    * Auto-Configure: `-e VIRTUAL_HOST=yourdomain.jp`
* MySQL Container
    * version: `5.6`
    * host: `localhost` (or `172.17.42.1`)
    * port: `33061`
    * Root-User: `root`
    * Root-Password: `root`