: > /var/lib/jenkins/nginx/conf.d/deployed.conf
docker ps -aq --filter "name=deployed-" | while read f; do
  upstream=`docker inspect --format '{{range .Config.Env}}{{println .}}{{end}}' $f | grep VIRTUAL_HOST= | sed 's/^VIRTUAL_HOST=//'`
  server=`docker inspect --format '{{range .Config.Env}}{{println .}}{{end}}' $f | grep DEPLOY_HOST= | sed 's/^DEPLOY_HOST=//'`
  cat << EOF >> /var/lib/jenkins/nginx/conf.d/deployed.conf
server {
    server_name ${server};
    listen 80 ;
    access_log /var/log/nginx/access.log vhost;
    location / {
        proxy_pass http://${upstream};
    }
}
EOF
done
docker exec -i nginx-proxy service nginx reload