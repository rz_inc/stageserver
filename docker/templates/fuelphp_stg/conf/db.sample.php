<?php
return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=DB_HOST;port=DB_PORT;dbname=DB_NAME',
			'username'   => 'DB_USER',
			'password'   => 'DB_PASS',
		),
	),
);