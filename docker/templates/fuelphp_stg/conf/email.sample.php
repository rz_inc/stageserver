<?php

return array(

	'defaults' => array(
		/**
		 * Email charset
		 */
		'charset' => 'ISO-2022-JP',
		//'charset' => 'UTF-8', 

		/**
		 * Wether to encode subject and recipient names.
		 * Requires the mbstring extension: http://www.php.net/manual/en/ref.mbstring.php
		 */
		'encode_headers' => true,

		/**
		 * Ecoding (8bit, base64 or quoted-printable)
		 */
		'encoding' => '7bit',
		//'encoding' => '8bit',

		/**
		 * Default sender details
		 */
		'from'		=> array(
			'email'		=> 'SENDER_EMAIL@me.com',
			'name'		=> 'SENDER_NAME'
		),
	),

);
