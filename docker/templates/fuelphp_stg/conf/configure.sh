#!/bin/bash

cp /home/projects/project/fuel/app/config/sample/* fuel/app/config/$FUEL_ENV/
sed -i "s/DB_NAME/$DBNAME/"    /home/projects/project/fuel/app/config/$FUEL_ENV/db.php
sed -i "s/DB_HOST/$DB_HOST/"   /home/projects/project/fuel/app/config/$FUEL_ENV/db.php
sed -i "s/DB_PORT/$DB_PORT/"   /home/projects/project/fuel/app/config/$FUEL_ENV/db.php
sed -i "s/DB_USER/$DB_USER/"   /home/projects/project/fuel/app/config/$FUEL_ENV/db.php
sed -i "s/DB_PASS/$DB_PASS/"   /home/projects/project/fuel/app/config/$FUEL_ENV/db.php
sed -i "s/SENDER_EMAIL@me.com/$SENDER_EMAIL/" /home/projects/project/fuel/app/config/$FUEL_ENV/email.php
sed -i "s/SENDER_NAME/$SENDER_NAME/"          /home/projects/project/fuel/app/config/$FUEL_ENV/email.php
sed -i "s/CRYPTO_KEY/$CRYPTO_KEY/"   /home/projects/project/fuel/app/config/$FUEL_ENV/crypt.php
sed -i "s/CRYPTO_IV/$CRYPTO_IV/"     /home/projects/project/fuel/app/config/$FUEL_ENV/crypt.php
sed -i "s/CRYPTO_HMAC/$CRYPTO_HMAC/" /home/projects/project/fuel/app/config/$FUEL_ENV/crypt.php
